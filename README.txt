 KeyStash NRS Login Plugin
 ===================


 Overview
 --------------------
 This plugin enables you to login via KeyStash(api) and the NXT NRS Wallet client.
 NOTE: All versions with "e" in it will be recommended to use with "test NXT".


 Running software
 --------------------
 Copy all files in your NRS client folder and use the given structure. You don't have to replace anything.
 Open the html files:
	- NXT_Wallet_KeyStash_Login.html 
	or
	- NXT_Wallet_KeyStash_Login_Testnet.html
 
 NOTE: NRS must be started and KeyStash api enabled.


 Customization:
 --------------------
 Default logout time is set to 5 minutes. This can be changed on every login.


 Technical details:
 --------------------
 The plugin uses the KeyStash api in order to login: http://localhost:7801/keystash
 
 Want to contribute?
 --------------------
 - create pull requests
 - review pull requests
 - review existing code
 - create issues
 - answer issues

