var apiUrl = 'http://localhost:7801/keystash?requestType=getAccounts';
var primaryPassword = null;
var accountId = null;
var accountFormValue = null;
var salt = null;
var iv = null;
var secret = null;
var iteration = null;
var keySize = null;
var logoutTime = null;
var uniLogoutTime = null;


function keystashListAccounts() {

    $.ajax({type: 'GET', url: apiUrl, async: false, jsonpCallback: 'keystash', contentType: "application/json", dataType: 'jsonp', timeout: 30000,
        success: function (json) {
            if (json.account.length == 0) {
                document.getElementById('keystash_accounts').innerHTML += '<option value="0">"You have no accounts in your wallet."</option>';
            }
            for (i = 0; i < json.account.length; i++) {
                if (i < 9) {
                    if (json.account[i].label == 'No Label') {
                        document.getElementById('keystash_accounts').innerHTML += '<option value="' + i + '">' + '0' + json.account[i].accountId + '. ' + json.account[i].addressRS + '</option>';
                    } else {
                        document.getElementById('keystash_accounts').innerHTML += '<option value="' + i + '">' + '0' + json.account[i].accountId + '. ' + json.account[i].addressRS + ' - ' + json.account[i].label + '</option>';
                    }
                } else {
                    document.getElementById('keystash_accounts').innerHTML += '<option value="' + i + '">' + json.account[i].accountId + '. ' + json.account[i].addressRS + '</option>';
                }
            }
        },
        error: function (json) {
            document.getElementById('keystash_accounts').innerHTML += '<option value="0">"You must enable the KeyStash api."</option>';
            $.growl("<strong>Warning: </strong>You must enable the KeyStash api.", {"type": "danger", "offset": 10});
        }
    });

}

function keystashDecrypt() {

    primaryPassword = document.getElementById("login_password").value;

    if (primaryPassword.length != 0) {
        $.growl("<strong>Decrypting: </strong>Please wait a momemt while processing.", {"type": "info", "offset": 10, "delay": 2000});
    }
    setTimeout(function () {
        keystashDecryptLogin();
    }, 1000);

}

function keystashDecryptLogin() {
    
    var uniLogoutTime = window.location.search;
    uniLogoutTime = uniLogoutTime.replace('?logoutTime=', '');
    getAccountId = document.getElementById("keystash_accounts");
    accountFormValue = getAccountId.options[getAccountId.selectedIndex].value;
    primaryPassword = document.getElementById("login_password").value;

    $.ajax({type: 'GET', url: apiUrl, async: false, jsonpCallback: 'keystash', contentType: "application/json", dataType: 'jsonp', timeout: 30000,
        success: function (json) {
            salt = json.account[accountFormValue].randomSalt;
            iv = json.account[accountFormValue].randomIV;
            secret = json.account[accountFormValue].secret;
            iteration = json.account[accountFormValue].secretIteration;
            keySize = json.account[accountFormValue].secretKeySize;
        }, error: function (json) {
            $.growl("<strong>Warning: </strong>You must enable the KeyStash api.", {"type": "danger", "offset": 10});
        }
    })
            .then(function () {
                if (primaryPassword.length == 0) {
                    $.growl("<strong>Warning: </strong>You must enter your primary password.", {"type": "danger", "offset": 10});
                } else {
                    try {
                        keySize = keySize / 32;
                        var decryptedPassphrase = keystashAesDecrypt(salt, iv, primaryPassword, secret, keySize, iteration);
                        NRS.loginkeystash(decryptedPassphrase);
                        if (uniLogoutTime == 0) {
                            logoutTime = 300000;
                        } else if (uniLogoutTime != 0) {
                            logoutTime = uniLogoutTime * 60000;
                        }
                        keystashLogout(logoutTime);
                        setTimeout(function () {
                            $.growl("<strong>Note: </strong>You have been successfully logged in.", {"type": "success", "offset": 60});
                        }, 1500);
                        setTimeout(function () {
                            if (logoutTime == 60000) {
                                $.growl("<strong>Note: </strong>You will be automatically logged out after " + logoutTime / 60000 + " minute.", {"type": "info", "offset": 60});
                            } else {
                                $.growl("<strong>Note: </strong>You will be automatically logged out after " + logoutTime / 60000 + " minutes.", {"type": "info", "offset": 60});
                            }
                        }, 3500);
                    } catch (e) {
                        $.growl("<strong>Warning: </strong>Your primary password is not correct.", {"type": "danger", "offset": 10});
                    }
                }
            });

}

function keystashLogout(logoutTime) {
    
    var t = logoutTime;
    var d = false;

    setTimeout(function () {
        $.growl("<strong>Note: </strong>You will now automatically get logged out...", {"type": "info", "offset": 60, "delay": 11000});
        d = true;
    }, t - 12000);
    setTimeout(function () {
        NRS.logout();
    }, t);

}

function keystashAesDecrypt(salt, iv, password, cipherText, keySize, iterations) {

    var s = keySize;
    var i = iterations;

    $("#loading").show();
    var key = CryptoJS.PBKDF2(password, CryptoJS.enc.Hex.parse(salt), {keySize: s, iterations: i});
    var cipherParams = CryptoJS.lib.CipherParams.create({
        ciphertext: CryptoJS.enc.Base64.parse(cipherText)
    });
    var decrypted = CryptoJS.AES.decrypt(
            cipherParams,
            key,
            {iv: CryptoJS.enc.Hex.parse(iv)});
    return decrypted.toString(CryptoJS.enc.Utf8);

}

